---
title: "My First Post"
date: 2022-07-21T10:50:08+08:00
draft: false
---

This is a test of generating my own blog using Hugo in golang.

> Drafts do not get deployed; once finished a post, update the header of the post to say `draft: false`.
